import { useEffect, useState } from 'react';
import { Flex, Spacer, Text } from '@chakra-ui/react'
import { CardanoWallet, useAddress, useWallet } from '@meshsdk/react';
import ConnectWallet from './walletButton';

export default function Footer() {
  const { wallet, connected, name, connecting, connect, disconnect, error } = useWallet();
  const [footerColor, setFooterColor] = useState('orange.200');
  const address = useAddress();

  useEffect(() => {
    if(connected){
      setFooterColor('purple.200')
    }
    else {
      setFooterColor('orange.200')
    }
  }, [connected])

  return (
      <Flex pos="fixed" bottom="0" direction="row" w="100%" p="5" bg={footerColor}>
        { connected ? (
          <Text>Connected to {name}</Text>
        ) : (
          <Text>Connect a Wallet</Text>
        )}
        <Spacer />
        { connected && address}
        <Spacer />
        <ConnectWallet />
      </Flex>
  )
}
