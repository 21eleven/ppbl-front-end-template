import { useQuery, gql } from "@apollo/client";

import {
    Flex, Center, Heading, Text, Box, Link, Button
} from "@chakra-ui/react";
import { useState } from "react";
import { useWallet } from "@meshsdk/react";

// ------------------------------------------------------------------------
// Module 302, Mastery Assignment #3
//
// STEP 1: Replace this GraphQL query with a new one that you write.
//
// Need some ideas? We will brainstorm at Live Coding.
// ------------------------------------------------------------------------
const QUERY = gql`
query TxAtAddress($address: String) {
    transactions(
      where: {
        _or: [
          { inputs: { address: { _eq: $address } } }
          { outputs: { address: { _eq: $address } } }
        ]
      }
    ) {
      hash
      inputs {
        address
        tokens {
          quantity
        }
      }
      outputs {
        tokens {
          quantity
        }
      }
      fee
      metadata {
        key
      }
      mint {
        quantity
      }
      scripts {
        type
      }
    }
  }
`;

type AddressInfo = {
    totalTx: number,
    txTokens: number,
    txMinting: number,
    txScripts: number
    txMetadata: number,
    feePaid: number
}

function utxosHaveTokens(utxos: any): boolean {
    for (var i=0; i < utxos.length; i++) {
        if (utxos[i].tokens.length > 0) {
            return true
        }
    }
    return false
}

function calculateAddressInfo(transactions: any, address: string): AddressInfo {
    let info: AddressInfo = {
        totalTx: 0,
        txTokens: 0,
        txMinting: 0,
        txScripts: 0,
        txMetadata: 0,
        feePaid: 0
    }
    transactions.forEach((tx: any) => {
        info.totalTx += 1
        if (tx.mint.length !== 0) {
            info.txMinting += 1
            info.txTokens += 1
        } else if (utxosHaveTokens(tx.inputs) || utxosHaveTokens(tx.outputs)) {
            info.txTokens += 1
        }
        for (var i=0; i < tx.scripts.length; i++) {
            if (tx.scripts[i].type.slice(0,6) === "plutus")
                info.txScripts += 1  
                break
        }
        if (tx.metadata.length !== 0) {
            info.txMetadata += 1
        }
        for (var i=0; i < tx.inputs.length; i++) {
            if (tx.inputs[i].address === address)
                info.feePaid += tx.fee    
                break
        }
    })
    return info
}


export default function Mastery302dot3Fruffolo() {

    const myAddress = "addr_test1qz5s85w86qfu4uway8d8l0hynv8uzsz5quph78x35wvl76k9g9wzq0hrrs4pzpuepdnjujfr0evyzhhvgj4x6cg8s29qvwfldj"

    const { connected, wallet } = useWallet();
    const [ queryAddress, setQueryAddress] = useState<string>(myAddress)

    const getUserInfo = async () => {
        if (connected) {
            setQueryAddress((await wallet.getUsedAddresses())[0])
        }
        else {
            alert("Please connect a wallet")
        }
    }

    const { data, loading, error } = useQuery(QUERY, {
        variables: {
            address: queryAddress
        }
    });

    if (loading) {
        return (
            <Heading size="lg">Loading data...</Heading>
        );
    };

    if (error) {
        console.error(error);
        return (
            <Heading size="lg">Error loading data...</Heading>
        );
    };

    // ------------------------------------------------------------------------
    // Module 302, Mastery Assignment #3
    //
    // STEP 2: Style your query results here.
    //
    // This template is designed be a simple example - add as much custom
    // styling as you want!
    // ------------------------------------------------------------------------

    const addressInfo = calculateAddressInfo(data.transactions, queryAddress)

    return (
        <Box p="3" bg="orange.100" border='1px' borderRadius='lg'>
            <Heading py='2' size='md'>Fruffolo Mastery Assignment 302.3</Heading>
            <Text p='1' fontWeight='bold'> Some stats of my personal Pre-Prod address:</Text>
            <Text p='1'>Address: <a href={"https://preprod.cardanoscan.io/address/".concat(queryAddress)}>{queryAddress.slice(0,15).concat(".....").concat(queryAddress.slice(98, 108))}</a></Text>
            <Text p='1'>Transaction count: {addressInfo.totalTx}</Text>
            <Text p='1'>Txs with Native Assets: {addressInfo.txTokens}</Text>
            <Text p='1'>Minting transactions: {addressInfo.txMinting}</Text>
            <Text p='1'>Txs with Metadata: {addressInfo.txMetadata}</Text>
            <Text p='1' pb='0'>Txs with Plutus Scripts: {addressInfo.txScripts}</Text>
            <Text pb='1' pl='1' pr='1'>(Minting policies and contracts)</Text>
            <Text pt='1' pl='1' pr='1'>Total transaction fees paid:  {addressInfo.feePaid / 1000000} t₳</Text>
            <Text pb='1' pl='1' pr='1'>(Only when address in inputs)</Text>
            <Button colorScheme={'purple'} onClick={getUserInfo} mt='2' py='2' size='lg' mb='5px' width={'full'}>See stats of your address</Button>
        </Box>
    )
}
