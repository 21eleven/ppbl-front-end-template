// Imports:
// You may not use all of these, and you may need to add a few!
import { useState } from "react";
import { useForm } from "react-hook-form";
import {
    Box, Heading, Text, Input, FormControl, Button, Center, Spinner, Link, FormLabel, NumberInput, NumberInputField
} from "@chakra-ui/react"
import { ErrorMessage, useFormik } from "formik";
import { Transaction, BrowserWallet } from '@meshsdk/core'
import type { UTxO } from "@meshsdk/core";
import { useWallet } from "@meshsdk/react";
import RegisteredFaucets from "../../pages/module301-faucets/registered-faucets";
import { resolve } from "path";



export default function TransactionMladen() {
    // These will come in handy:
    const { connected, wallet } = useWallet();
    const [successfulTxHash, setSuccessfulTxHash] = useState<string | null>(null)
    const [loading, setLoading] = useState(false);

    const {
        handleSubmit,
        register,
        formState: { errors, isSubmitting }
    } = useForm();

    function onSubmit(values: {
        address(address: any, message: any): unknown; message: any; lovelace: any; 
    }) {
        return new Promise<void>((resolve) => {
          setTimeout(async () => {
            const wallet = await BrowserWallet.enable("eternl");
            const tx = new Transaction({ initiator: wallet })
              .sendLovelace(
                values.address,
                "2000000"
              );
            tx.setMetadata(0, values.message)
            const unsignedTx = await tx.build();
            const signedTx = await wallet.signTx(unsignedTx);
            const txHash = await wallet.submitTx(signedTx);
            alert(txHash) ;
    
    
            alert(JSON.stringify(values, null, 2));
            alert(values.address);
            resolve();
          }, 300);
        });
      }

    return (
        <Box p='5' bg='orange.100' border='1px' borderRadius='xl' fontSize='lg'>
            <Heading size='lg'>Send ADA with some metadata</Heading>
            <form onSubmit={handleSubmit(onSubmit)}>
            <FormControl isInvalid={errors.address}>
                <FormLabel htmlFor="address">
                    Reciver
                </FormLabel>
                <Input 
                    id="address"
                    bg="white"
                    placeholder="address"
                    {...register("address", {
                        required: "Cannot be empty"
                    })} 
                />
                <FormLabel htmlFor="amount">Message to send</FormLabel>
                <Input 
                    id="message"
                    bg="white"
                    placeholder="Your Message"
                    {...register("message", {
                        required: "Please input your message"
                    })} 
                />
            </FormControl>
            <Button mt={4} colorScheme="teal" isLoading={isSubmitting} type="submit">
                Submit
            </Button>
        </form>
        </Box>
    );
}