import { useState } from "react";
import { Box, Heading, Text, Input, Button, Center, Spinner } from "@chakra-ui/react"
import { Transaction } from '@meshsdk/core'
import { useWallet } from "@meshsdk/react";


type Recipient = {
    address: string,
    lovelace: string
}
function newRecipient(): Recipient{
    let newRecipient: Recipient = {address:"", lovelace:""}
    return newRecipient
}


export default function TransactionFruffolo() {

    const { connected, wallet } = useWallet();
    const [successfulTxHash, setSuccessfulTxHash] = useState<string | null>(null)
    const [loading, setLoading] = useState(false);
    const [recipients, setRecipients] = useState([newRecipient()])

    const addRecipient = async () => {
        setRecipients( recs => recs.concat(newRecipient()))
    }

    const handleTransaction = async () => {
        if (connected) {
            setLoading(true)
            const network = await wallet.getNetworkId()
            if (network == 1) {
                alert("For now, this dapp only works on Cardano Testnet")
            } else {
                const tx = new Transaction({ initiator: wallet })
                for (var i=0; i < recipients.length; i++) {
                    tx.sendLovelace(
                        recipients[i].address,
                        recipients[i].lovelace
                )}
                try {
                    const unsignedTx = await tx.build();
                    const signedTx = await wallet.signTx(unsignedTx);
                    const txHash = await wallet.submitTx(signedTx);
                    console.log("Submitted tx: ", txHash)
                    setSuccessfulTxHash(txHash)
                } catch (error: any) {
                    if (error.info) {
                        alert(error.info)
                    }
                    else {
                        console.log(error)
                    }
                }
            }
            setLoading(false)
        }
        else {
            alert("Please connect a wallet")
        }
    }

    return (
        <Box p='5' bg='orange.100' border='1px' borderRadius='xl' fontSize='lg'>
            <Heading mb='3' size='xl'>
                Multi-address Sender
            </Heading>
            {recipients.map( (recipient: Recipient, index:number ) => (
                <Box key={"recipient".concat(index.toString())}>
                    <Input mb='1' key={"addr".concat(index.toString())} bg='white' placeholder="Recipient address" value={recipient.address} onChange={ (event: any) => {
                        let newRec: Recipient[] = Array.from(recipients)
                        recipients[index].address = event.target.value
                        setRecipients(newRec)
                }} />
                    <Input mb='3' key={"q".concat(index.toString())} bg='white' placeholder="Lovelace amount" value={recipient.lovelace} onChange={ (event: any) => {
                        let newRec: Recipient[] = Array.from(recipients)
                        recipients[index].lovelace = event.target.value
                        setRecipients(newRec)
                }} />
                </Box>
            ))}
            <Button w='100%' colorScheme='purple' onClick={addRecipient}>Add recipient</Button>        
            {loading ? (
                <Center p='3'>
                    <Spinner size='md' speed="1.0s"/>
                </Center>
            ) : (
                <Button my='3' w='100%' colorScheme='purple' onClick={handleTransaction}>Send! 🚀</Button>
            )}
            <Box mt='1' p='2' bg='blue.100'>
                <Heading size='sm' py='1'>Status</Heading>
                {successfulTxHash ? (
                    <Text>Successful tx: <a href={"https://preprod.cardanoscan.io/transaction/".concat(successfulTxHash)}>{successfulTxHash.slice(0,8).concat(".....").concat(successfulTxHash.slice(57, 64))}</a></Text>
                ) : (
                    <Text>Ready to test a transaction!</Text>
                )}
            </Box>
        </Box>
    );
}