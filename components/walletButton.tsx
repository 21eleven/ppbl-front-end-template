import { useWallet, useWalletList } from '@meshsdk/react';

import { Button } from '@chakra-ui/react';

export default function ConnectWallet() {
  const { wallet, connected, name, connecting, connect, disconnect, error } = useWallet();
  const wallets = useWalletList()

  return (
    <>
      {connected ? (
        <Button onClick={() => disconnect()} colorScheme='orange'>
          Disconnect Wallet
        </Button>
      ) : (
        wallets.map((w, i) => (
          <Button key={i} onClick={() => connect(w.name)} colorScheme='purple'>
            Connect with {w.name}
          </Button>
        ))
      )}
    </>
  );
}
